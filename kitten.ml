(* A comment on type inference for concatenative languages;
   inspired by a post of Jon Purdy: 
     http://evincarofautumn.blogspot.fr/2013/07/static-typing-in-concatenative-language.html

   I don't believe that inferring "{drop} {drop} compose" is
   difficult. It should be reasonably easy with a type-checker
   dedicated to concatenative languages. In fact, you don't even need
   to implement a type checker yourself: you can encode concatenative
   languages into functional languages, and use the type inference
   engine of the host language directly. The encoding is simple
   enough, I hope, to be a convincing argument that type inference
   would be doable directly on the input language.


   (wholly unoriginal) idea: encode stack languages into functional
   languages by representing the stack as nested tuples. If a stack
   has type ('a), pushing a value of type 'b will return a stack of
   type ('a * 'b); then pushing a ('c) will give (('a * 'b) *
   'c). Note that the type variable 'a here represent a stack type
   (capitalized in Cat), while 'b and 'c only represent types of
   values pushed on the stack.

   The stack effect noted
   (Char Char -> Bool)
   will therefore be represented by the OCaml type
   (('a * char) * char) -> ('a * bool)
*)
module Lib = struct
  let binary ((stack, v1), v2) f = (stack, f v1 v2)
  let unary (stack, v) f = (stack, f v)
  
  let add_float s = binary s (+.)
  let add_int s = binary s (+)
  let add_vector s = binary s (@)
  let and_bool s = binary s (&&)
  let and_int s = binary s (land)
  let apply (stack, f) = f stack
  let bottom s = unary s (fun li -> List.hd (List.rev li))
  (* Close became close_in and close_out *)
  let close_in s = unary s Pervasives.close_in
  let close_out s = unary s Pervasives.close_out
  let dec_float s = unary s (fun x -> x -. 1.)
  let dec_int s = unary s (fun n -> n - 1)
  let div_float s = binary s (/.)
  let div_int s = binary s (/)
  let down s = unary s List.tl
  let drop (s, v) = s
  let dup (s, v) = ((s, v), v)
  let empty s = unary s (function [] -> true | _::_ -> false)
  let eq_char s = binary s (fun (c1:char) c2 -> c1 = c2)
  let eq_float s = binary s (fun (x1:float) x2 -> x1 = x2)
  let eq_int s = binary s (fun (n1:int) n2 -> n1 = n2)
  let exit s = unary s Pervasives.exit
  let first s = unary s fst
  let rest s = unary s snd
  let function_ s = unary s (fun v stack -> (stack, v))
  let ge_char s = binary s (fun (c1:char) c2 -> c1 >= c2)
  let ge_float s = binary s (fun (x1:float) x2 -> x1 >= x2)
  let ge_int s = binary s (fun (n1:int) n2 -> n1 >= n2)
  let get s = binary s (fun li n -> List.nth n li)
  let get_line s = unary s input_line
  let gt_char s = binary s (fun (c1:char) c2 -> c1 > c2)
  let gt_float s = binary s (fun (x1:float) x2 -> x1 > x2)
  let gt_int s = binary s (fun (n1:int) n2 -> n1 > n2)
  let inc_float s = unary s (fun x -> x +. 1.)
  let inc_int s = unary s (fun n -> n + 1)
  let le_char s = binary s (fun (c1:char) c2 -> c1 <= c2)
  let le_float s = binary s (fun (x1:float) x2 -> x1 <= x2)
  let le_int s = binary s (fun (n1:int) n2 -> n1 <= n2)
  let length s = unary s List.length
  let lt_char s = binary s (fun (c1:char) c2 -> c1 < c2)
  let lt_float s = binary s (fun (x1:float) x2 -> x1 < x2)
  let lt_int s = binary s (fun (n1:int) n2 -> n1 < n2)
  let mod_float s = binary s Pervasives.mod_float
  let mod_int s = binary s (mod)
  let mul_float s = binary s ( *. )
  let mul_int s = binary s ( * )
  let ne_char s = binary s (fun (c1:char) c2 -> c1 <> c2)
  let ne_float s = binary s (fun (x1:float) x2 -> x1 <> x2)
  let ne_int s = binary s (fun (n1:int) n2 -> n1 <> n2)
  let not_bool s = unary s (not)
  (* NotInt ? *)
  let open_in s = unary s Pervasives.open_in
  let open_out s = unary s Pervasives.open_out
  let or_bool s = binary s (||)
  let or_int s = binary s (lor)
  let set s = binary s (fun li v n ->
    let tab = Array.of_list li in
    tab.(n) <- v;
    Array.to_list tab
  )
  let show_float s = unary s string_of_float
  let show_int s = unary s string_of_int
  let stderr stack = (stack, Pervasives.stderr)
  let stdin stack = (stack, Pervasives.stdin)
  let stdout stack = (stack, Pervasives.stdout)
  let sub_float s = binary s (-.)
  let sub_int s = binary s (-)
  let print ((stack, str), output) =
    output_string output str;
    stack
  let swap ((stack, v1), v2) = ((stack, v2), v1)
  let top s = unary s List.hd
  let up s = unary s (fun li -> List.rev (List.tl (List.rev li)))
  let vector s = unary s (fun v -> [v])
  let xor_bool s = binary s (fun b1 b2 -> b1 || b2 && not (b1 && b2))
  let xor_int s = binary s (lxor)
  let call f stack = f stack
  let compose ((stack, f), g) = (stack, fun s -> g (f s))
  let if_ cond_ then_ else_ (stack, b) = if b then then_ stack else else_ stack
  let push v stack = (stack, v)
  let scoped f (stack, v) = f v stack
  
  let ($) stack f = f stack
  let (!) = push
end

(* Remark: we use the combinator ($) for sequencing stack actions
   (an operation which is implicit in concatenative languages), and
   (!)  for pushing values onto the stack (an operation that is
   implicit for values in Kitten, but explicitly represented by
   quotation {foo} for functions)

   There is a well-known trick to make ($) implicit in functional
   languages, that correspond to a specific form of
   continuation-passing-style. This allows a lighter syntax for the
   examples (directly resembling the concatenative equivalent), but
   makes the code a bit more complex and type errors harder to
   follow. Or idea here is more to show how to "compile"
   a concatenative program into a language using Hindley-Milner type
   inference -- so it's ok if the syntax is a bit less convenient.

   These syntactic sugar hack have been explained in even more complex
   situations (LR parsing!) in the 2003 Functional Pearl from Ralf Hinze,
   "Typed Quote/Antiquote"
     http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.67.4417
 *)


(* test/int.ktn *)
open Lib

let unary s = s $ !1
let binary s = s $ !1 $ !2

let print s = s $ stdout $ print
let newline s = s $ !"\n" $ print
let sayi s = s $ show_int $ print $ newline
let ii s = s $ scoped (fun f s -> s $ unary $ !f $ apply $ sayi)
let iii s = s $ scoped (fun f s -> s $ binary $ !f $ apply $ sayi)

let () = () $ !add_int $ iii

(* note that getting the code wrong results in a type inference error:


    (* forgot to quote `add_int`:
       it cannot apply on the empty stack *)
    let () = () $ add_int $ iii
    Error: This expression has type ('a * int) * int -> 'a * int
           but an expression was expected of type unit -> 'b

    (* forgot to apply `iii`:
       the result doesn't have the type `unit` of the empty stack *)
    let () = () $ !add_int
    Error: This expression has type unit * (('a * int) * int -> 'a * int)
       but an expression was expected of type unit
*)


(* also note that the ML type inference engine is perfectly able to infer
   higher-order functions *)
let drop2 s = s $ !drop $ !drop $ compose
(* let drop2 s = s $ !drop $ !drop $ compose *)

let swap_fun s = s $ scoped (fun f s -> s $ swap $ f)
let swap_fun s = s $ scoped (fun f s -> s $ swap $ !f $ apply)
(* both are equivalent and:
   val swap_fun : (('a * 'b) * 'c) * (('a * 'c) * 'b -> 'd) -> 'd *)
